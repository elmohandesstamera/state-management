import 'package:flutter/material.dart';
import 'package:quran/config/router/routes.dart';
import 'package:quran/core/widgets/main_navigation/main_navigation_view.dart';
import 'package:quran/features/splash/splash_view.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.splash:
       return MaterialPageRoute(builder: (context) =>const SplashView(),);
      case Routes.home:
        return MaterialPageRoute(builder: (context) => const MainNavigationView());
      default:
        return MaterialPageRoute(builder: (context) => Scaffold(
          appBar: AppBar(
          ),
          body: const Center(
            child: Text("No Route Defined"),
          ),
        ));
    }
  }
}
