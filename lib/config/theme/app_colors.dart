import 'package:flutter/material.dart';

class AppColor{
  AppColor._();
  static const Color blueColor=Color(0xff0056e5);
  static const Color lightGrey=Color(0xfff3f3f4);
  static const Color deepGrey=Color(0xff9f9f9f);


}