import 'package:flutter/material.dart';
import 'package:quran/config/theme/app_colors.dart';

class DarkThemeColors{
  DarkThemeColors._();
  
  static const Color lightScaffoldLightDenimColor=Color(0xff22245d);
  static const Color darkScaffoldDarkDenimColor=Color(0xff10113d);
  static const Color defaultScaffoldBackgroundColor=Color(0xff22245d);
  static const Color primaryColor=AppColor.blueColor;
  static const Color surfaceColor=Color(0xff23255e);
  static const Color onSurfaceColor=Color(0xffffffff);
  static const Color onPrimaryColor=Color(0xffffffff);
  static const Color borderColor=Color(0xff23255e);
  static const Color errorColor=Color(0xffd32f2f);
  static const Color onErrorColor=Color(0xffffffff);
  static const Color secondary=Color(0xff696e98);
  static const Color onSecondary=Color(0xffffffff);
}