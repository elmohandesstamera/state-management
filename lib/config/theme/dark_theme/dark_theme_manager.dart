import 'package:flutter/material.dart';
import 'package:quran/config/theme/dark_theme/dark_theme_colors.dart';

class DarkThemeManger {
 static ThemeData darkTheme = ThemeData(
      primaryColor: DarkThemeColors.primaryColor,
      scaffoldBackgroundColor: DarkThemeColors.defaultScaffoldBackgroundColor,
      //add border color

      buttonTheme: ButtonThemeData(
        buttonColor: DarkThemeColors.primaryColor,
        
      ),
      colorScheme: const ColorScheme(
        // bor
        brightness: Brightness.dark,
        primary: DarkThemeColors.primaryColor,
        onPrimary: DarkThemeColors.onPrimaryColor,
        secondary: DarkThemeColors.secondary,
        onSecondary: DarkThemeColors.onSecondary,
        error: DarkThemeColors.errorColor,
        onError: DarkThemeColors.onErrorColor,
        surface: DarkThemeColors.surfaceColor,
        onSurface: DarkThemeColors.onSurfaceColor,
      ));
}
