import 'package:flutter/material.dart';
import 'package:quran/config/theme/app_colors.dart';

class LightThemeColors{

  static const Color scaffoldBackgroundColor=Color(0xfff3f5f8);
  static const Color primaryColor=AppColor.blueColor;
  static const Color surfaceColor=Color(0xffffffff);
  static const Color onSurfaceColor=Color(0xff000000);
  static const Color onPrimaryColor=Color(0xffffffff);
  static const Color borderColor=Color(0xffe0e0e0);
  static const Color errorColor=Color(0xffd32f2f);
  static const Color onErrorColor=Color(0xffffffff);
  static const Color secondary=Color(0xff696e98);
  static const Color onSecondary=Color(0xffffffff);
  
}