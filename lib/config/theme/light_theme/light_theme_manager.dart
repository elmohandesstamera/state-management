
import 'package:flutter/material.dart';
import 'package:quran/config/theme/light_theme/light_theme_colors.dart';

class  LightThemeManager {
   
   static ThemeData lightTheme=ThemeData(
       primaryColor: LightThemeColors.primaryColor,
        scaffoldBackgroundColor: LightThemeColors.scaffoldBackgroundColor,
        colorScheme: ColorScheme.light(
          primary: LightThemeColors.primaryColor,
          surface: LightThemeColors.surfaceColor,
          onSurface: LightThemeColors.onSurfaceColor,
          onPrimary: LightThemeColors.onPrimaryColor,
          secondary: LightThemeColors.secondary,
          onSecondary: LightThemeColors.onSecondary,
          error: LightThemeColors.errorColor,
          onError: LightThemeColors.onErrorColor,
        ),
        bottomAppBarTheme: BottomAppBarTheme(
          color: LightThemeColors.surfaceColor
        )
   );
}