class AppConstants {
  // Private constructor to prevent instantiation
  AppConstants._();

  static const String lamaFont = "lama_arabic";
}
