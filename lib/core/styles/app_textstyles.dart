

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quran/config/theme/app_colors.dart';
import 'package:quran/core/constants/app_constants.dart';
import 'package:quran/core/helper/font_weight_helper.dart';

class AppTextstyles{

  // Arabic Text Styles
  static final TextStyle lama14BlueRegular = TextStyle(
    fontSize: 14.sp,
    fontWeight: FontWeightHelper.regular,
    fontFamily: AppConstants.lamaFont,
    color: AppColor.blueColor
  );
    final TextStyle? lama14BlueBold = TextStyle(
    fontSize: 14.sp,
    fontWeight: FontWeightHelper.bold,
    fontFamily: AppConstants.lamaFont,
    color: AppColor.blueColor
  );
   final TextStyle? lama14BlueExtraBold = TextStyle(
    fontSize: 14.sp,
    fontWeight: FontWeightHelper.extraBold,
    fontFamily: AppConstants.lamaFont,
    color: AppColor.blueColor
  );
  static final TextStyle lama13DeepGreyRegular= TextStyle(
    fontSize: 13.sp,
    fontWeight: FontWeightHelper.regular,
    fontFamily: AppConstants.lamaFont,
    color: AppColor.deepGrey
  );

}