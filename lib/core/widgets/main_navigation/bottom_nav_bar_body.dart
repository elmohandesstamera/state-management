import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quran/config/theme/app_colors.dart';
import 'package:quran/core/styles/app_textstyles.dart';

class BottomNavBarBody extends StatefulWidget {
  const BottomNavBarBody(
      {super.key, required this.currentIndex, required this.onTap});
  final Function(int)? onTap;
  final int currentIndex;

  @override
  State<BottomNavBarBody> createState() => _BottomNavBarBodyState();
}

class _BottomNavBarBodyState extends State<BottomNavBarBody> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60.h,
      child: Theme(
        data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          bottomNavigationBarTheme: const BottomNavigationBarThemeData(
            backgroundColor: Colors.white,
            selectedItemColor: AppColor.blueColor,
            unselectedItemColor: AppColor.deepGrey,
          ),
        ),
        child: BottomNavigationBar(
          elevation: 0,
          onTap: widget.onTap,
          selectedLabelStyle: AppTextstyles.lama14BlueRegular,
    
          unselectedLabelStyle: AppTextstyles.lama13DeepGreyRegular,
     
     
          selectedIconTheme: const IconThemeData(
            color: AppColor.blueColor,
          ),
          unselectedIconTheme: const IconThemeData(
            color: AppColor.deepGrey,
          ),
          landscapeLayout: BottomNavigationBarLandscapeLayout.linear,
          currentIndex: widget.currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                widget.currentIndex == 0 ? Icons.search : Icons.security,
                size: 30,
              ),
              label: 'سيسي',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                widget.currentIndex == 1 ? Icons.search : Icons.security,
                size: 30,
              ),
              label: 'المحفوظات',
            ),
            BottomNavigationBarItem(
              icon: FaIcon(
                widget.currentIndex == 2
                    ? FontAwesomeIcons.house
                    : FontAwesomeIcons.houseChimneyCrack,
                size: 25,
              ),
              label: 'الرئيسية',
            ),
          ],
        ),
      ),
    );
  }
}
