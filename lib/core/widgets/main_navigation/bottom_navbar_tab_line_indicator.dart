
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quran/config/theme/app_colors.dart';

class BottomNavBarTabLineIndicator extends StatelessWidget {
  const BottomNavBarTabLineIndicator({super.key,
  required this.currentIndex
  });
  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    // Tab width can be calculated by dividing the screen width by the number of tabs
    double eachTabWidth = screenWidth / 3;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: AnimatedContainer(
        margin: EdgeInsets.only(
          left: currentIndex * eachTabWidth,
          right: (2 - currentIndex) * eachTabWidth,
        ),
        duration: const Duration(milliseconds: 300),
        curve: Curves.fastOutSlowIn,
        width: eachTabWidth,
        decoration:  BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10.r),
            bottomRight: Radius.circular(10.r),
          ),
          color: AppColor.blueColor,
        ),
        height: 4.h,
      ),
    );
  }
}
