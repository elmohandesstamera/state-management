import 'package:flutter/material.dart';
import 'package:quran/core/styles/app_textstyles.dart';
import 'package:quran/core/widgets/main_navigation/bottom_nav_bar_body.dart';
import 'package:quran/core/widgets/main_navigation/bottom_navbar_tab_line_indicator.dart';
import 'package:quran/features/home/presentation/widgets/home_view_body.dart';

class MainNavigationView extends StatefulWidget {
  const MainNavigationView({super.key});

  @override
  State<MainNavigationView> createState() => _MainNavigationViewState();
}

class _MainNavigationViewState extends State<MainNavigationView> {
  int currentIndex = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: currentIndex,
        children: [
          const Center(child: Text('search')),
          const Center(child: Text('bookmark')),
          HomeViewBody()
        ],
      ),
      bottomNavigationBar: Stack(
        children: [
          // The bottom navigation bar
          BottomNavBarBody(
            currentIndex: currentIndex,
            onTap: (index) {
              setState(() {
                currentIndex = index;
              });
            },
          ),
          // Line indicator for the selected tab
          // on the top of The BottomNavBar
          BottomNavBarTabLineIndicator(
            currentIndex: currentIndex,
          )
        ],
      ),
    );
  }
}
