import 'package:flutter/material.dart';
import 'package:quran/config/router/routes.dart';
import 'package:quran/core/helper/extensions.dart';
import 'package:quran/features/splash/widgets/splash_view_body.dart';

class SplashView extends StatefulWidget {
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    delayedNavigation();
    super.initState();
  }

  // Delayed Navigation
  delayedNavigation() async {
    await Future.delayed(const Duration(milliseconds: 3000));
    navigateToHomeView();
  }

  // Navigate to HomeView
  navigateToHomeView() {
    context.navigateToAndReplace(Routes.home);
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(body: SplashViewBody());
  }
}
