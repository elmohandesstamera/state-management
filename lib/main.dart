import 'package:flutter/material.dart';
import 'package:quran/yaqeen_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const YaqeenApp());
}
