import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quran/config/router/app_router.dart';
import 'package:quran/config/router/routes.dart';
import 'package:quran/config/theme/dark_theme/dark_theme_manager.dart';
import 'package:quran/config/theme/light_theme/light_theme_manager.dart';

class YaqeenApp extends StatelessWidget {
  const YaqeenApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MaterialApp(
          title: 'Yaqeen App',
          // change the app language
          locale: const Locale('ar'),
          
          debugShowCheckedModeBanner:false,
          theme: LightThemeManager.lightTheme,
          onGenerateRoute: AppRouter().onGenerateRoute,
          initialRoute: Routes.splash,
        );
      },
      child: Container()
    );
  }
}

